# cd-wod

I wanted to do the normal thing I'd do and scrape this using a parser, in ruby. Nokogiri I guess.

But the weirdly formatted markup defeated Nokogiri, so I defaulted to simple tools for a simple task.

Note that this now seems to work for an arbitrary date, e.g.: https://api-v4.concept2.com/wod/2024-06-21
